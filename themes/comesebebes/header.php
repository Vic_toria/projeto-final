<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/style.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/painel.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Bellota+Text:ital,wght@0,300;0,400;0,700;1,300;1,400&family=Darker+Grotesque:wght@300;400;500&family=Roboto:wght@300&display=swap" rel="stylesheet">
    <title><?php bloginfo('name') ?> | <?php the_title(); ?></title>
</head>
<body class="body-page-login">

<header class="header">
<div class="container-header">
            <figure class="logo-page">
                <a href="http://comesbebes.local/home/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/Vector.png"></a>
            </figure>
            <?php get_product_search_form(); ?>
            <h4 class="title-request-header">Faça um pedido</h4>
            <figure class="logo-cart">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/Vector (1).png">
            </figure>
            <figure class="logo-perfil">
                <a href="http://comesbebes.local/minha-conta/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/Vector (2).png"></a>
            </figure>
        </div>
</header>
 
