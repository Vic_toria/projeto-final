<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/b18674122a.js" crossorigin="anonymous"></script>
    <link href="<?php echo get_stylesheet_directory_uri() ?>/style.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/painel.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Bellota+Text:ital,wght@0,300;0,400;0,700;1,300;1,400&family=Darker+Grotesque:wght@300;400;500&family=Roboto:wght@300&display=swap" rel="stylesheet">
    <title>Document</title>
</head>
<body class="body-page-login">
        <div class="container-header-nav">
            <a href="http://comesbebes.local/minha-conta/"><h3 class="painel">Painel</h3></a>
            <a href="http://comesbebes.local/minha-conta/pedido/"><h3 class="pedidos">Pedidos</h3></a>
            <a href="http://comesbebes.local/minha-conta/endereco/"><h3 class="endereco">Endereços</h3></a>
            <a href="http://comesbebes.local/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fcomesbebes23.local%2Fusuario%2F&amp;_wpnonce=08e341f003"><h3 class="sair">Sair</h3></a>
        </div>