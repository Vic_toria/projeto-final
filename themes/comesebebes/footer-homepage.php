<script src="https://kit.fontawesome.com/b18674122a.js" crossorigin="anonymous"></script>
<link href="footer_homepagecss.css" rel="stylesheet"/>
<link href="Normalize.css" rel="stylesheet"/>
<footer class="footer_homepage">
    <h1 class="title_footer-homepage">VISITE NOSSA LOJA FÍSICA</h1>
    <div class="container_principal_footer"> 
        <div class="container_location_footer-homepage">
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.7537673028!2d-43.1332584!3d-22.9064193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1660776718787!5m2!1spt-BR!2sbr"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="information_footer-homepage">
                <p><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/icon_restaurant.png"></i>Rua lorem ipsum, 123, LI, Brasil</p>
                <p><i class="fa-solid fa-phone"></i>(XX) XXXX-XXXX</p>
            </div>
        </div>
        <figure class="image_footer-homepage">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/foto_restaurante.png">
        </figure>
    </div>
    <div class="copyright">
        <p>© Copyright 2022 - Desenvolvido por Turma da Mônica</p>
    </div>
</footer>