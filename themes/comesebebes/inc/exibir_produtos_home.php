<?php

function exibir_categorias($product_categories) {
    
    ?>
    <div class="container_apresentation">
        <h1 class="title_apresentation">Comes&Bebes</h1>
        <h2 class="subtitle_apresentation">O restaurante para todas as fomes</h2>
    </div> 
    <main class="main_ladding"> 
        <div class="ladding_principal">
            <div class="title_welcome"><h1>CONHEÇA NOSSA LOJA</h1></div>
            <div class="title_pratos_principais">
                <p>Tipos de pratos principais</p>
            </div>
            <div class="ladding_images_category">
            <?php foreach ($product_categories as $category) { ?>
                <div class="box1_ladding">
                    <picture class="imagem1">
                    <?php 
                        $thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true ); 
                        $image = wp_get_attachment_url( $thumbnail_id );
                        echo "<img src='{$image}' alt='' width='240px' height='373px' />"
                    ?>
                        <div class="name_category">
                            <h2>
                                <?php 
                                    echo $category->name;     
                                ?>
                            <h2>
                        </div>
                    </picture>
                </div>
                <?php } ?>
            </div>
    

   <?php }


function pegar_categorias() {
    $taxonomy     = 'product_cat';
    $orderby      = 'name'; 

    $args = array('taxonomy' => $taxonomy, 'orderby' => $orderby);

    return get_categories( $args );
}

function pratosDoDia($diaDaSemana) {
  $args  = [
    'limit' => 4,
    'orderby' => 'date',
    'order' => 'DESC',
    'tag' => $diaDaSemana
  ];
  return wc_get_products($args);
}

function dateTime() {
    $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
    $data = date('Y-m-d');
    $diasemana_numero = date('w', strtotime($data));
    return $diasemana[$diasemana_numero];
}

function exibe_pratos_dia($pratosdodia) {
    ?>
        <div class="conatainer_titulo_prato-do-dia"> 
            <p>Pratos do dia de hoje: </p>
            <p class="dia_semana"><?php echo dateTime(); ?></p>
        </div>
        <div class="div_produtos_semana">
        <?php foreach($pratosdodia as $prato) {?>
        <a href="<?= $prato->get_permalink(); ?>">
        <div>
            <picture>
            <?= $prato->get_image(); ?>
            </picture>
            <div class="name_cart_price">
                <div>
                    <p><?= $prato->get_name(); ?></p>
                </div>
                <div class="cart_and_price">
                    <p><?= $prato->get_price_html(); ?></p>
                    <picture>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img header/icone_carrinho.png" alt="iconecarrinho">
                    </picture>
                </div>
            </div>
        </div>
        </a>
    <?php } ?>
        </div>
    <!--Botão de ver outras opções-->
    <div class="button_ladding">
        <a href="http://comesbebes.local/loja/"><button>veja outras opções</button></a>
    </div>
    <?php
} 
?>