<?php


function exibir_categorias_menu($categorias) {
    ?>
    <nav>
        <?php 
        foreach ($categorias as $categoria){
        ?>
        <a href="/categoria-produto/<?= $categoria->slug ?>">
            <picture>
                <?php 
                    $thumbnail_id = get_term_meta( $categoria->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                    echo "<img src='{$image}' alt='' width='240px' height='373px' />" ?>
            </picture>
            <div>
                <?= $categoria->name; ?>
            </div>
        </a>
    <?php } ?>
    </nav>
    <?php
}




?>