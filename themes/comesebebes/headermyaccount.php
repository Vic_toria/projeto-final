<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/style.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/painel.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Bellota+Text:ital,wght@0,300;0,400;0,700;1,300;1,400&family=Darker+Grotesque:wght@300;400;500&family=Roboto:wght@300&display=swap" rel="stylesheet">
    <title>Document</title>
</head>
<body class="body-page-login">
    <header class="header">
        <div class="container-header">
            <figure class="logo-page">
            <img src="assets/images/img header/Vector.png">
            </figure>
            <input type="text" placeholder="Sashmi" class="search-bar-header"></input>
            <h4 class="title-request-header">Faça um pedido</h4>
            <figure class="logo-cart">
            <img src="assets/images/img header/Vector (1).png">
            </figure>
            <figure class="logo-perfil">
            <img src="assets/images/img header/Vector (2).png">
            </figure>
        </div>
        <div class="container-header-nav">
            <h3 class="painel">Painel</h3>
            <h3 class="pedidos">Pedidos</h3>
            <h3 class="endereco">Endereços</h3>
            <h3 class="sair">Sair</h3>
        </div>
    </header>