<?php 

function estilo_personalizado() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'comesebebes' );
}
add_action( 'after_setup_theme', 'estilo_personalizado');

function comesebebes_css(){
    wp_register_style( 'comesebebes', get_template_directory_uri() . '/' . 'style.css', [], '1.0.0' );
    wp_enqueue_style( 'comesebebes', get_template_directory_uri() . '/' . 'style.css', [], '1.0.0');
}
add_action( 'wp_enqueue_scripts', 'comesebebes_css');

require "inc/exibir_produtos_home.php";
require "inc/categorias.php";
require "inc/produto.php";

function my_theme_scripts_function() {
  wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js');
}
add_action('wp_enqueue_scripts','my_theme_scripts_function');



function action_woocommerce_customer_save_address( $user_id, $load_address ) { 
    wp_safe_redirect(wc_get_page_permalink('myaccount')); 
    exit;
 }; 
 add_action( 'woocommerce_customer_save_address', 'action_woocommerce_customer_save_address', 99, 2 );

 add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );

 function custom_override_billing_fields( $fields ) {
   unset($fields['billing_phone']);
   unset($fields['billing_email']);
   unset($fields['billing_company_field']);
   unset($fields['billing_address_nickname']);
   return $fields; 
 }

 add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );

 function custom_override_shipping_fields( $fields ) {
    unset($fields['shipping_phone']);
    unset($fields['shipping_email']);
    unset($fields['shipping_address_nickname']);
    unset($fields['shipping_company_field']);
    return $fields; 
  }

  
  register_nav_menu( 'wp_nav_menu', 'Categorias' );

function comes_loop_shop_per_page(){
    return 8;
  }
  
add_filter('loop_shop_per_page', 'comes_loop_shop_per_page');



?>